//
//  SBImageRecognition.h
//  Created by Vladimir Ozerov on 25/04/16.
//  Copyright © 2016 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MocrEngine.h"


/** @brief Режим распознавания изображения */
typedef NS_ENUM(uint8_t, SBImageRecognitionMode) {
	SBImageRecognitionModeText = 0,
	SBImageRecognitionModeBusinessCard,
	SBImageRecognitionModeBarcode,
};


/** @brief Ошибки и ворнинги */
extern NSString * NSStringFromTMocrErrorCode(TMocrErrorCode code);
extern NSString * NSStringFromTMocrWarningCode(TMocrWarningCode code);

/** @brief Тип поля в результате распознавания визитки */
extern NSString * NSStringFromTMocrBcrFieldType(TMocrBcrFieldType fType);
extern NSString * NSStringFromTMocrBcrFieldComponentType(TMocrBcrFieldComponentType cType);