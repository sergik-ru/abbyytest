//
//  SBImageRecognitionManager.h
//  Created by Vladimir Ozerov on 22/04/16.
//  Copyright © 2016 SberTech. All rights reserved.
//

#import "SBImageRecognition.h"



/** @brief Контейнер для результата распознавания */
@interface SBImageRecognitionResult : NSObject

@property (nonatomic, assign, readonly) TMocrRotationType rotationType;                /**< Поворот исходного изображения */
@property (nullable, nonatomic, strong, readonly) CMocrLayout *mocrLayout;             /**< Результат распознавания текста */
@property (nullable, nonatomic, strong, readonly) CMocrBusinessCard *mocrBusinessCard; /**< Результат распознавания визитки */
@property (nullable, nonatomic, strong, readonly) CMocrBarcode *mocrBarcode;           /**< Результат распознавания штрихкода */

@property (nullable, nonatomic, readonly) NSDictionary *dictionaryRepresentation; /**< Представление в виде словаря */

@end



@class SBImageRecognitionManager;



@protocol SBImageRecognitionManagerDelegate <NSObject>
@optional
/** @brief Процент выполнения распознавания
 *  @discussion Вызывется на главном потоке. Предназначен для отображения в UI прогресса при распознавании изображения.
 *  @param progress Процент выполнения, от 0 до 1
 */
- (void)recognitionManager:(SBImageRecognitionManager * _Nonnull)manager recognitionProgressChanged:(CGFloat)progress;
@required
- (void)recognitionManager:(SBImageRecognitionManager * _Nonnull)manager succeedWithResult:(SBImageRecognitionResult * _Nonnull)result image:(UIImage * _Nonnull)image;
- (void)recognitionManager:(SBImageRecognitionManager * _Nonnull)manager failedWithCode:(TMocrErrorCode)errorCode message:(NSString * _Nonnull)errorMessage;
@end



@interface SBImageRecognitionManager : NSObject

@property (nonatomic, readonly) BOOL busy; /**< Происходит распознавание одного или нескольких изображений */
@property (nullable, nonatomic, weak) id<SBImageRecognitionManagerDelegate> delegate;

/** @brief Распознать изображение
 *  @discussion Выполняется асинхронно. Результат передается через делегат
 */
- (void)recognizeImage:(UIImage * _Nonnull)image inMode:(SBImageRecognitionMode)mode;

/** @brief Stop recognition
 *  @discussion Stops all active recognition operations
 */
- (void)stopAllRecognitionTasks;

@end
