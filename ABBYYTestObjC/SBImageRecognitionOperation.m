//
//  SBImageRecognitionOperation.m
//  Created by Vladimir Ozerov on 25/04/16.
//  Copyright © 2016 SberTech. All rights reserved.
//

#import "SBImageRecognitionOperation.h"


@interface SBImageRecognitionOperation () <IMocrRecognitionCallback>

@property (nonatomic, assign) SBImageRecognitionMode mode;
@property (nonnull, nonatomic, strong) NSObject<IMocrRecognitionManager> *recognitionManager;
@property (nonnull, nonatomic, strong) UIImage *image;

@end


@implementation SBImageRecognitionOperation

+ (instancetype)operationWithMode:(SBImageRecognitionMode)mode recognitionManager:(NSObject<IMocrRecognitionManager> *)recognitionManager image:(UIImage *)image
{
	NSAssert(recognitionManager && image, @"Отсутствует обязательный параметр (%@, %@)", recognitionManager, image);

	SBImageRecognitionOperation *op = [SBImageRecognitionOperation new];
	op.mode = mode;
	op.recognitionManager = recognitionManager;
	op.image = image;
	return op;
}

- (void)main
{
	if (self.cancelled) { return; }

	// Здесь происходит распознавание изображения
	id layout;
	TMocrRotationType rotationType;
	BOOL result = NO;
	switch (_mode)
	{
		case SBImageRecognitionModeText:
			result = [_recognitionManager recognizeTextOnImage:_image withCallback:self storeLayout:(CMocrLayout **)&layout rotation:&rotationType];
			break;
		case SBImageRecognitionModeBusinessCard:
			result = [_recognitionManager recognizeBusinessCardOnImage:_image withCallback:self storeBusinessCard:(CMocrBusinessCard **)&layout rotation:&rotationType];
			break;
		case SBImageRecognitionModeBarcode:
			result = [_recognitionManager recognizeBarcodeOnImage:_image withCallback:self storeBarcode:(CMocrBarcode **)&layout];
			break;
	}

    __weak SBImageRecognitionOperation *weakSelf = self;
	dispatch_async(dispatch_get_main_queue(), ^()
	{
        if (weakSelf.cancelled) { return; }
        
        if (result)
		{
			[_delegate recognitionOperation:weakSelf succeedWithLayout:layout rotationType:rotationType image:self.image];
		}
		else
		{
			TMocrErrorCode errorCode;
			NSString* errorMessage;
			[CMocrEngine getLastError:&errorCode message:&errorMessage];

			[_delegate recognitionOperation:weakSelf failedWithCode:errorCode message:errorMessage];
		}
	});
}


#pragma mark - IMocrRecognitionCallback

- (BOOL)calledWithProgress:(int)progress warning:(TMocrWarningCode)warning
{
	if (self.cancelled) { return FALSE; }
	//NSLog(@"%i", progress);
	if (warning != MWC_NoWarning)
		//NSLog(@"%@", NSStringFromTMocrWarningCode(warning));
	
	dispatch_async(dispatch_get_main_queue(), ^()
	{
		[_delegate recognitionOperation:self progressChanged:((CGFloat)progress / 100)];
	});
	
	return YES;
}

- (void)onRotationTypeDetected:(TMocrRotationType)rotationType
{
	//NSLog(@"%i", rotationType);
}

- (void)onPrebuiltWordsInfoReady:(CMocrPrebuiltLayoutInfo *)layoutInfo
{
	//NSLog(@"%@", layoutInfo);
}

#pragma mark - override

- (BOOL)isConcurrent
{
	return YES;
}


@end
