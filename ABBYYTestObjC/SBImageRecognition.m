//
//  SBImageRecognition.m
//  Created by Vladimir Ozerov on 25/04/16.
//  Copyright © 2016 SberTech. All rights reserved.
//

#import "SBImageRecognition.h"


NSString * NSStringFromTMocrErrorCode(TMocrErrorCode code)
{
	static NSString * const ErrorPrefix = @"ABBYY.Error.";
	NSString *errorString = nil;
	switch (code)
	{
		case MEC_NoError:                     errorString = @"None";                        break;
		case MEC_EngineInstanceExists:        errorString = @"EngineInstanceExists";        break;
		case MEC_EngineNotInitialized:        errorString = @"EngineNotInitialized";        break;
		case MEC_FileNotFound:                errorString = @"FileNotFound";                break;
		case MEC_InvalidArgument:             errorString = @"InvalidArgument";             break;
		case MEC_MemoryAllocationFailed:      errorString = @"MemoryAllocationFailed";      break;
		case MEC_RecognitionInProgress:       errorString = @"RecognitionInProgress";       break;
		case MEC_FineErrNotInitialized:       errorString = @"FineErrNotInitialized";       break;
		case MEC_FineErrLicenseError:         errorString = @"FineErrLicenseError";         break;
		case MEC_FineErrInvalidArgument:      errorString = @"FineErrInvalidArgument";      break;
		case MEC_FineErrInternalFailure:      errorString = @"FineErrInternalFailure";      break;
		case MEC_FineErrNotEnoughMemory:      errorString = @"FineErrNotEnoughMemory";      break;
		case MEC_FineErrTerminatedByCallback: errorString = @"FineErrTerminatedByCallback"; break;
	}
	return [ErrorPrefix stringByAppendingString:errorString];
}

NSString * NSStringFromTMocrWarningCode(TMocrWarningCode code)
{
	static NSString * const WarningPrefix = @"ABBYY.Warning.";
	NSString *warningString = nil;
	switch (code)
	{
		case MWC_NoWarning:              warningString = @"None";                   break;
		case MWC_SlowRecognition:        warningString = @"SlowRecognition";        break;
		case MWC_ProbablyBadImage:       warningString = @"ProbablyBadImage";       break;
		case MWC_ProbablyWrongLanguages: warningString = @"ProbablyWrongLanguages"; break;
		case MWC_SureWrongLanguages:     warningString = @"SureWrongLanguages";     break;
	}
	return [WarningPrefix stringByAppendingString:warningString];
}

NSString * NSStringFromTMocrBcrFieldType(TMocrBcrFieldType fType)
{
	switch (fType)
	{
		case MBFT_Phone:
			return @"Phone";
		case MBFT_Fax:
			return @"Fax";
		case MBFT_Mobile:
			return @"Mobile";
		case MBFT_Email:
			return @"Email";
		case MBFT_Web:
			return @"Web";
		case MBFT_Address:
			return @"Address";
		case MBFT_Name:
			return @"Name";
		case MBFT_Company:
			return @"Company";
		case MBFT_Job:
			return @"Job";
		case MBFT_Text:
			return @"Other";
		default:
			return @"...";
	}
}

NSString * NSStringFromTMocrBcrFieldComponentType(TMocrBcrFieldComponentType cType)
{
	switch (cType)
	{
		case MBFCT_FirstName:
			return @"First Name";
		case MBFCT_MiddleName:
			return @"Middle Name";
		case MBFCT_LastName:
			return @"Last Name";
		case MBFCT_ExtraName:
			return @"Extra Name";
		case MBFCT_Title:
			return @"Title";
		case MBFCT_Degree:
			return @"Degree";
		
		case MBFCT_PhonePrefix:
			return @"Phone Prefix";
		case MBFCT_PhoneCountryCode:
			return @"Phone Country Code";
		case MBFCT_PhoneCode:
			return @"Phone Code";
		case MBFCT_PhoneBody:
			return @"Phone Body";
		case MBFCT_PhoneExtension:
			return @"Phone Extension";
		
		case MBFCT_ZipCode:
			return @"Zip Code";
		case MBFCT_Country:
			return @"Country";
		case MBFCT_City:
			return @"City";
		case MBFCT_StreetAddress:
			return @"Street Address";
		case MBFCT_Region:
			return @"Region";
		
		case MBFCT_JobPosition:
			return @"Job Position";
		case MBFCT_JobDepartment:
			return @"Job Department";
		default:
			return @"...";
	}
}
