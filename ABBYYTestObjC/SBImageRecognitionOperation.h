//
//  SBImageRecognitionOperation.h
//  Created by Vladimir Ozerov on 25/04/16.
//  Copyright © 2016 SberTech. All rights reserved.
//

#import "SBImageRecognition.h"


@class SBImageRecognitionOperation;


@protocol SBImageRecognitionOperationDelegate <NSObject>
@required
- (void)recognitionOperation:(SBImageRecognitionOperation * _Nonnull)operation progressChanged:(CGFloat)progress;
- (void)recognitionOperation:(SBImageRecognitionOperation * _Nonnull)operation succeedWithLayout:(id _Nonnull)layout rotationType:(TMocrRotationType)rotationType image:(UIImage * _Nonnull)image;
- (void)recognitionOperation:(SBImageRecognitionOperation * _Nonnull)operation failedWithCode:(TMocrErrorCode)errorCode message:(NSString * _Nonnull)errorMessage;
@end


@interface SBImageRecognitionOperation : NSOperation

@property (nonatomic, assign, readonly) SBImageRecognitionMode mode;
@property (nullable, nonatomic, weak) id<SBImageRecognitionOperationDelegate> delegate;

+ (instancetype _Nonnull)operationWithMode:(SBImageRecognitionMode)mode recognitionManager:(NSObject<IMocrRecognitionManager> * _Nonnull)recognitionManager image:(UIImage * _Nonnull)image;

@end
