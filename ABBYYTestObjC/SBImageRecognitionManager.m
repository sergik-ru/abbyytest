//
//  SBImageRecognitionManager.m
//  Created by Vladimir Ozerov on 22/04/16.
//  Copyright © 2016 SberTech. All rights reserved.
//

#import "SBImageRecognitionManager.h"
#import "SBImageRecognitionOperation.h"


static NSString * const AppID = @"ru.sberbank.mydocuments";


@interface SBImageRecognitionResult ()

@property (nonatomic, assign) TMocrRotationType rotationType;
@property (nullable, nonatomic, strong) CMocrLayout *mocrLayout;
@property (nullable, nonatomic, strong) CMocrBusinessCard *mocrBusinessCard;
@property (nullable, nonatomic, strong) CMocrBarcode *mocrBarcode;

@end



@interface SBImageRecognitionManager () <SBImageRecognitionOperationDelegate>
@end



@implementation SBImageRecognitionManager
{
	CMocrEngine *engine;
	NSOperationQueue *recognitionQueue;
}

- (id)init
{
	self = [super init];
	if (self)
	{
		engine = [CMocrEngine getSharedEngine];
		if (!engine)
		{
			NSString *bundlePath = [[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:@"ABBYY.bundle"];
			NSBundle *abbyyBundle = [NSBundle bundleWithPath:bundlePath];
			
			NSString *licensePath = [abbyyBundle pathForResource:@"ABBYY.license" ofType:nil];
			NSData *licenseData = [NSData dataWithContentsOfFile:licensePath];
			
			CMocrDirectoryDataSource *dataSource = [CMocrDirectoryDataSource dataSourceWithDirectoryPath:abbyyBundle.bundlePath];
			CMocrLicense *license = [CMocrLicense licenseWithLicenseData:licenseData applicationId:AppID];
			engine = [CMocrEngine createArcSharedEngineWithDataSources:@[dataSource] license:license];
		}
		if (!engine)
		{
			TMocrErrorCode errorCode;
			NSString* errorMessage;
			[CMocrEngine getLastError:&errorCode message:&errorMessage];
			NSLog(@"%@ (%@)", NSStringFromTMocrErrorCode(errorCode), errorMessage);
			
			// Отменяем инициализацию
			return nil;
		}
		recognitionQueue = [NSOperationQueue new];
		recognitionQueue.maxConcurrentOperationCount = 5;
	}
	return self;
}


#pragma mark - Properties

- (BOOL)busy
{
	return !!recognitionQueue.operationCount;
}


#pragma mark - Actions

- (void)recognizeImage:(UIImage *)image inMode:(SBImageRecognitionMode)mode
{
	NSSet *langs = [NSSet setWithObjects:@"English", @"Russian", nil];
	CMocrRecognitionConfiguration * config =
	[[CMocrRecognitionConfiguration alloc] initWithImageResolution:0
											imageProcessingOptions:MIPO_DisableImageGeometricTransform | MIPO_DetectPageOrientation
												   recognitionMode:MRM_Full
										recognitionConfidenceLevel:MRCL_Level3
													  barcodeTypes:0
												   defaultCodePage:MSCP_Utf8
													 unknownLetter:L'^'
											  recognitionLanguages:langs];
	
	NSObject<IMocrRecognitionManager> *recognitionManager = [engine newRecognitionManagerWithConfiguration:config];
	
	SBImageRecognitionOperation *operation = [SBImageRecognitionOperation operationWithMode:mode recognitionManager:recognitionManager image:image];
	operation.delegate = self;
	[recognitionQueue addOperation:operation];
}

- (void)stopAllRecognitionTasks
{
	[recognitionQueue cancelAllOperations];
}

#pragma mark - SBImageRecognitionOperationDelegate

- (void)recognitionOperation:(SBImageRecognitionOperation *)operation progressChanged:(CGFloat)progress
{
	if ([_delegate respondsToSelector:@selector(recognitionManager:recognitionProgressChanged:)])
		[_delegate recognitionManager:self recognitionProgressChanged:progress];
}

- (void)recognitionOperation:(SBImageRecognitionOperation *)operation succeedWithLayout:(id)layout rotationType:(TMocrRotationType)rotationType image:(UIImage *)image
{
	SBImageRecognitionResult *result = [SBImageRecognitionResult new];
	result.rotationType = rotationType;
	
	switch (operation.mode)
	{
		case SBImageRecognitionModeText:
			result.mocrLayout = layout;
			break;
		case SBImageRecognitionModeBusinessCard:
			result.mocrBusinessCard = layout;
			break;
		case SBImageRecognitionModeBarcode:
			result.mocrBarcode = layout;
			break;
	}
	
	[_delegate recognitionManager:self succeedWithResult:result image: image];
}

- (void)recognitionOperation:(SBImageRecognitionOperation *)operation failedWithCode:(TMocrErrorCode)errorCode message:(NSString *)errorMessage
{
	[_delegate recognitionManager:self failedWithCode:errorCode message:errorMessage];
}

@end



@implementation SBImageRecognitionResult

- (NSDictionary *)dictionaryRepresentation
{
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	if (_mocrLayout)
	{
		dict[@"text"] = ({
			NSString *text = [[_mocrLayout copyStrings] componentsJoinedByString:@"\n"];
			text ?: @"";
		});
	}
	if (_mocrBusinessCard)
	{
		for (CMocrBusinessCardField *bcrField in _mocrBusinessCard.fields)
		{
			NSString *key = NSStringFromTMocrBcrFieldType(bcrField.fieldType);
			NSString *value = ({
				id result = nil;
				BOOL isComplexField = !!bcrField.components.count;
				NSMutableDictionary *valueDict = isComplexField ? [NSMutableDictionary dictionary] : nil;
				
				// Простые строки поля
				NSMutableArray<NSString *> *lines = [NSMutableArray array];
				for (CMocrTextLine *textLine in bcrField.lines)
					[lines addObject:[textLine copyString]];
				
				// Компоненты поля
				if (isComplexField)
				{
					if (lines.count)
						valueDict[@"value"] = [lines componentsJoinedByString:@"\n"];
					
					for (CMocrBusinessCardFieldComponent *component in bcrField.components)
					{
						NSString *componentName = NSStringFromTMocrBcrFieldComponentType(component.componentType);
						
						NSMutableArray<NSString *> *subLines = [NSMutableArray array];
						for (CMocrTextLine *textLine in component.lines)
							[subLines addObject:[textLine copyString]];

						valueDict[componentName] = [subLines componentsJoinedByString:@"\n"];
					}
					result = valueDict;
				}
				else
				{
					result = [lines componentsJoinedByString:@"\n"];
				}
				result;
			});
			if (key && value)
				dict[key] = value;
		}
	}
	if (_mocrBarcode)
	{
		dict[@"barcode"] = [_mocrBarcode.text copyString] ?: @"";
	}
	return [dict copy];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"RecognitionResult(%i, %@, %@, %@)", (int)_rotationType, _mocrLayout, _mocrBusinessCard, _mocrBarcode];
}

@end
